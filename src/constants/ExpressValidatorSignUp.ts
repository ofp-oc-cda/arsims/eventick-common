import {body} from "express-validator";
import {PassLength} from "./constants";

export const ExpressValidatorSignUp = [
    body("username").notEmpty().withMessage("Username is required"),
    body("email").isEmail().withMessage("Email must be valid"),
    body("password")
        .trim()
        .notEmpty()
        .isLength({min: PassLength.min, max: PassLength.max})
        .withMessage(`Password must be between ${PassLength.min} and ${PassLength.max} characters`),
];
