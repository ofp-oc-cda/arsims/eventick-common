import { body } from "express-validator";

export const ExpressValidatorTicketUpdate = [
    body("title").not().isEmpty().withMessage("Title is required"),
    body("descripton").not().isEmpty().withMessage("Desccription is required"),
    body("price").isFloat({gt: 0}).withMessage("Price must be grater than 0"),
    body("category").not().isEmpty().withMessage("Category is required")
]