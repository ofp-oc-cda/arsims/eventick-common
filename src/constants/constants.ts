import {PasswordLength} from "../interfaces"

export const PassLength: PasswordLength = {
    min: 4,
    max: 20
}

export let request: any = null;

export const setSession = (session: any) => {
    request = session
    return request
}
