import { Model, Document } from "mongoose";

//Abstract Class for Error Handling better than Interfaces
export abstract class CustomError extends Error {
    abstract statusCode: number;

    constructor(message: string) {
        super(message);
        Object.setPrototypeOf(this, CustomError.prototype);
    }

    abstract serializeErrors(): {
        message: string;
        field?: string;
    }[];
}

//Interface for Error Handling
export interface CustomErrorHandler {
    statusCode: number;

    serializeErrors(): {
        message: string;
        field?: string;
    }[];
}

//Interface for User Model
export interface IUser {
    username?: string;
    email: string;
    password: string;
}

//An interface that describes the properties that a User Model can have
export interface IUserModel extends Model<IUserDoc> {
    build(attrs: IUser): IUserDoc;
}

// Interface that describes the properties that a User documents has
export interface IUserDoc extends Document {
    username?: string;
    email: string;
    password: string;
}

export interface PasswordLength {
    min: number;
    max: number;
}

export interface UserPayload {
    id: string;
    username: string;
    email: string;
}

export interface TicketAttrs {
    title: string;
    description: string;
    price: number,
    userId: string,
    category: string[]
}

// We can add here properties in the future
export interface TicketDoc extends Document {
    title: string;
    description: string;
    price: number,
    userId: string,
    category: string[]
}

export interface TicketModel extends Model<TicketDoc> {
    build(attrs: TicketAttrs): TicketDoc
}

//TODO: publish to npm registry to use to other services
