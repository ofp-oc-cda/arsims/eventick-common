import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import { UserPayload } from "../interfaces";
import dotenv from "dotenv";
import { request } from "../constants"

dotenv.config();

declare module "express-serve-static-core" {
  export interface Request {
    currentUser?: UserPayload;
  }
}

export const currentUser = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  console.log('Entering CurrentUser Middleware');
  try {
    const payload = jwt.verify(
      request,
      process.env.JWT_KEY!
    ) as UserPayload;
    req.currentUser = payload;
  } catch (err) {
    // Handle JWT verification errors
    console.error("Error verifying JWT:", err);
    return res.status(401).json({ error: "Invalid token" });
    // or return next(err);
  }

  next();
};
