import {Request, Response, NextFunction} from 'express';
import {NotAuthorizedError} from '../errors';
import {request} from "../constants"

export const RequireAuth = (req: Request, res: Response, next: NextFunction) => {
    console.log("Entering RequireAuth middleware");
    
    if (!request) {
        throw new NotAuthorizedError();
    }
    next()
}
